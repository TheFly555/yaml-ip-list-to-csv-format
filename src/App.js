import logo from './logo.svg';
import './App.css';
import React from 'react';
import { saveAs } from 'file-saver';

function App() {
  const [input, setInput] = React.useState('');
  const [output, setOutput] = React.useState('');
  let items = [];

  function trimAndRemoveSpaces(str) {
    const parts = str.trim().split(' #');
    if (!parts.some(str => str.startsWith('#'))) {
      items.push(`${parts[0].replace(/^-\s*/, '')},${parts[1]}`);
      return `${parts[0].replace(/^-\s*/, '')},${parts[1]}`;
    }
  }

  function handleSubmit(event) {
    event.preventDefault();
    const rows = input.split('\n');
    const processedRows = rows.map(trimAndRemoveSpaces);
    setOutput(processedRows.join('\n'));
    console.log();
    // Create a Blob (binary large object) from the CSV string
    const blob = new Blob([output.replace(/^\s*\n/gm, "")], { type: 'text/csv;charset=utf-8' });
      
    // Save the Blob as a file
    saveAs(blob, 'array.csv');
  }

  return (
    <form onSubmit={handleSubmit}>
      <textarea rows={50} cols={50} value={input} onChange={event => setInput(event.target.value)} />
      <textarea rows={50} cols={50} value={output.replace(/^\s*\n/gm, "")} readOnly />
      <button type="submit">Trim</button>
    </form>
  );
}

export default App;
